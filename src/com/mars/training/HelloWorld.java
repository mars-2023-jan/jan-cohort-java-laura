package com.mars.training;
import com.mars.training.Exception.*;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class HelloWorld {
	public static void main(String args[]) {
		//make a checked (compile time) exception
		// FileNotFoundException
		// IOException
		// SQLException
		// InterruptedException
		try {
			FileReader file = new FileReader("test.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		int x = 0;
		try {
			int y = 5/x; // error is being caught here
			int z = 67;
			if (y > 2) {
				throw new MyException("This is my custom exception");
			}
		}
		
		// this code throws custom exception above
		catch (ArithmeticException | NumberFormatException | MyException e ) {
			if (e instanceof MyException) {
				System.out.println(e.getMessage());
			}
			else if (e instanceof NumberFormatException) {
				System.out.println("Number Format Exception");
			}
		}
		finally {
			System.out.println("Finally Block");
			// here we will close any scanners or open variables connected to database
		}
		
		
//		// this code throws generic unchecked exception from JVM
//		// ArithmeticException
//		// ArrayIndexOutOfBoundException
//		// NumberFormatException
//		catch (ArithmeticException | NumberFormatException e ) {
//			System.out.println("Exception Occurred...");
//			if (e instanceof ArithmeticException) {
//				System.out.println("Arithmetic Exception");
//			}
//			else if (e instanceof NumberFormatException) {
//				System.out.println("Number Format Exception");
//			}
//		}
//		finally {
//			System.out.println("Finally Block");
//			// here we will close any scanners or open variables connected to database
//		}
		
// 		this is the old way - see new way above		
//		catch(ArithmeticException e) {
//			System.out.println("Arithmetic Exception Occurred");
//		}
//		catch(Exception e) {
//			System,out.println("Some Exception Occurred");
//		}
		System.out.println("Program Continues");
		
    }
}