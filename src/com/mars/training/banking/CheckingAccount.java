package com.mars.training.banking;

public class CheckingAccount extends Account{

	boolean isChoiceCard;
	boolean isChoiceChecks;
		
	public boolean isChoiceCard() {
		return isChoiceCard;
	}
	public void setChoiceCard(boolean isChoiceCard) {
		this.isChoiceCard = isChoiceCard;
	}
	public boolean isChoiceChecks() {
		return isChoiceChecks;
	}
	public void setChoiceChecks(boolean isChoiceChecks) {
		this.isChoiceChecks = isChoiceChecks;
	}
		
	@Override
	public String toString() {
		return  "Account Number: " + String.valueOf(getAcctNumber()) +
				" Account Holder Name: " + getAcctHolderName() +
				" Account Balance: " + String.valueOf(getAcctBalance()) +
				" Minimum Balance: " + String.valueOf(getMinBalance()) +
				" Bank Card: " + String.valueOf(isChoiceCard) +
				" Checking Account: " + String.valueOf(isChoiceChecks);
		}
		
}
