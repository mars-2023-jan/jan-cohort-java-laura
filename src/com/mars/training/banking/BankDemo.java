package com.mars.training.banking;

import java.util.Scanner;
import com.mars.training.Exception.MyException;
import java.util.InputMismatchException;
import java.util.Random;

public class BankDemo {

	public static void main(String[] args) {
		
	try {
		
		Scanner sc = new Scanner(System.in);
		
		int choiceMain = 0;
		
		do { // until person enters 1, 2, or 3
				
			System.out.println("Press 1 for Checking, Press 2 for Savings, Press 3 to Create New Account:");
			choiceMain = sc.nextInt();
			
			if (choiceMain == 1) {
				
				//do checking stuff
				CheckingAccount ca1 = new CheckingAccount();
				ca1.fakeLogin();
				float minBalance = 0; // minimum checking account balance is 0
				
				// this information would be stored in database and retrieved into the current fields from database
	
				int choice = 0;
				do { // until person enters 1, 2, or 3
					System.out.println("Enter 1 for Deposit or 2 for Withdrawal or 3 to Check Balance:");
					choice = sc.nextInt();
					
					if (choice == 1) {
						System.out.println("Enter the amount to deposit:");
						double amount = sc.nextDouble();
						if (amount <= 0) {
							throw new MyException("Custom Exception: You cannot enter a negative deposit amount.");
						}
						else {
							ca1.deposit(amount);
						}
					}
					
					else if (choice == 2) {
						System.out.println("Enter the amount to withdraw:");
						double amount = sc.nextDouble();
						if (amount <= 0) {
							throw new MyException("Custom Exception: You cannot enter a negative withdrawal amount.");
						}
						else {
							ca1.withdrawal(amount);
						}	
					}
					
				} while ((choice != 1) && (choice != 2) && (choice !=3));
				
				ca1.checkBalance();
			}
			
			else if (choiceMain == 2) {
				
				//do savings stuff
				SavingsAccount sa1 = new SavingsAccount();
				sa1.fakeLogin();
				float minBalance = 100; // minimum saving account balance is 100
				
				int choice = 0;
				do { // until person enters 1, 2, or 3
					System.out.println("Enter 1 for Deposit or 2 for Withdrawal or 3 to Check Balance:");
					choice = sc.nextInt();
					
					if (choice == 1) {
						System.out.println("Enter the amount to deposit:");
						double amount = sc.nextDouble();
						if (amount <= 0) {
							throw new MyException("Custom Exception: You cannot enter a negative deposit amount.");
						}
						else {
							sa1.deposit(amount);	
						}
					}
					
					else if (choice == 2) {
						System.out.println("Enter the amount to withdraw:");
						double amount = sc.nextDouble();
						if (amount <= 0) {
							throw new MyException("Custom Exception: You cannot enter a negative withdrawal amount.");
						}
						else {
							sa1.withdrawal(amount);
						}
					}
					
				} while ((choice != 1) && (choice != 2) && (choice !=3));
				
				sa1.checkBalance();
			}
			
			else if (choiceMain == 3) {
				
				// do create account stuff
				System.out.println("Thank you for choosing to bank with us!");
				
				int choiceSub = 0;
				
				do { // until they enter 1, 2, or 3
					System.out.println("Press 1 to Create Checking Account, Press 2 to Create savings Account, Press 3 to Create a Money Market Account:");
					choiceSub = sc.nextInt();
					
					if (choiceSub == 1) {
						
						//make a new checking account
						Random rnd = new Random();
						int acctNo = 100000 + rnd.nextInt(900000);
						
						System.out.println("Enter Account Holder First and Last Name:");
						String acctHolderName = sc.next() + sc.nextLine();
						
						System.out.println("Enter Initial Account Balance:");
						double acctBalance = sc.nextDouble();
						
						float minBalance = 0; // minimum checking account balance is 0
						
						System.out.println("Would you like a bank card? Press 1 for Yes, Press 2 for No:");
						int choiceCard = sc.nextInt();
						
						System.out.println("Would you like paper checks? Press 1 for Yes, Press 2 for No:");
						int choiceChecks = sc.nextInt();
							
						CheckingAccount ca1 = new CheckingAccount();
						ca1.setAcctNumber(acctNo);
						ca1.setAcctHolderName(acctHolderName);
						ca1.setAcctBalance(acctBalance);
						ca1.setMinBalance(minBalance);
						if (choiceCard == 1) {
							ca1.setChoiceCard(true);
						}
						else if (choiceCard == 2) {
							ca1.setChoiceCard(false);
						}
						if (choiceChecks == 1) {
							ca1.setChoiceChecks(true);
						}
						else if (choiceChecks == 2) {
							ca1.setChoiceChecks(false);
						}
						
						System.out.println("Thank you for creating a new Checking account with the following information:");
						System.out.println(ca1);
										
					}
					
					else if (choiceSub == 2) {
						
						//make a new savings account
						Random rnd = new Random();
						int acctNo = 100000 + rnd.nextInt(900000);
						
						float minBalance = 100; // minimum savings account balance is 100
						
						float rate = 3; //interest rate for savings account is 3%
						
						System.out.println("Enter Account Holder First and Last Name:");
						String acctHolderName = sc.next() + sc.nextLine();
						
						double acctBalance = 0;
						do { //until they enter an initial amount above minimum
							System.out.println("Enter Initial Account Balance of at least $100:");
							acctBalance = sc.nextDouble();
						} while (acctBalance < minBalance);
						
						SavingsAccount sa1 = new SavingsAccount();
						sa1.setAcctNumber(acctNo);
						sa1.setAcctHolderName(acctHolderName);
						sa1.setAcctBalance(acctBalance);
						sa1.setMinBalance(minBalance);
						sa1.setRoi(rate);
						
						System.out.println("Thank you for creating a new Savings account with the following information:");
						System.out.println(sa1);
					}
					
					else if (choiceSub == 3) {
						
						//make a new money market account
						Random rnd = new Random();
						int acctNo = 100000 + rnd.nextInt(900000);
						
						float minBalance = 1000; // minimum money market account balance is 1000
						
						System.out.println("Enter Account Holder First and Last Name:");
						String acctHolderName = sc.next() + sc.nextLine();
						
						double acctBalance = 0;
						do { //until they enter an initial amount above minimum
							System.out.println("Enter Initial Account Balance of at least $1000:");
							acctBalance = sc.nextDouble();
						} while (acctBalance < minBalance);
						
						int whichAcct = 0;
						do { //until they press 1 or 2
							System.out.println("Would you like to transfer Account Balance from Checking or Savings?");
							System.out.println("Press 1 for Checking, Press 2 for Savings:");
							whichAcct = sc.nextInt();
						} while ((whichAcct != 1) && (whichAcct != 2));
						
						if (whichAcct == 1) {
							//debit from checking (process withdrawal)
							System.out.println("You are debiting " + acctBalance + " from checking");
							
						}
						else if (whichAcct == 2) {
							//debit from savings (process withdrawal)
							System.out.println("You are debiting " + acctBalance + " from savings");
						}
						
						int years = 0;
						do { //until they enter 1, 3 or 5 years
							System.out.println("Enter your Term (in Years) Choose 1, 3 or 5:");
							years = sc.nextInt();
						} while ((years != 1) && (years != 3) && (years != 5));
						
						MoneyMarketAccount mm1 = new MoneyMarketAccount();
						mm1.setAcctNumber(acctNo);
						mm1.setAcctHolderName(acctHolderName);
						mm1.setAcctBalance(acctBalance);
						mm1.setMinBalance(minBalance);
						mm1.setYears(years);
						mm1.setRoi(years);
						
						System.out.println("Thank you for creating a new Money Market account with the following information:");
						System.out.println(mm1);
						mm1.calcReturn(acctBalance, years);
						
					}
				
				}	while ((choiceSub != 1) && (choiceSub != 2) && (choiceSub != 3));
			} 
		} while ((choiceMain != 1) && (choiceMain != 2) && (choiceMain !=3));
	} 
	catch (InputMismatchException | MyException e ) {
		if (e instanceof MyException) {
			System.out.println(e.getMessage());
		}
		else if (e instanceof InputMismatchException) {
			System.out.println("Custom Exception: Program was expecting a numerical entry.");
		}
	}
	finally {
		// here we will close any scanners or open variables connected to database
	}
	}

}
	
