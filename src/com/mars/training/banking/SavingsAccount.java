package com.mars.training.banking;

public class SavingsAccount extends Account {
	
	private float roi;

	
	public float getRoi() {
		return roi;
	}
	public void setRoi(float roi) {
		this.roi = roi;
	}

	@Override
	public String toString() {
		return  "Account Number: " + String.valueOf(getAcctNumber()) +
				" Account Holder Name: " + getAcctHolderName() +
				" Account Balance: " + String.valueOf(getAcctBalance()) +
				" Minimum Balance: " + String.valueOf(getMinBalance()) +
				" Interest Rate: " + String.valueOf(getRoi());
	}
	
	////// added for exception handling example
//	@Override
//	public double calculate(int roi, int n) throws ArithmeticException {
//		return (12.5*roi)/n;
//	}
	///////////////////////////////////////////
}
