package com.mars.training.banking;

public class MoneyMarketAccount extends Account{
	
	private float roi;
	private int years;
	public float getRoi() {
		return roi;
	}
	public void setRoi(int year) {
		if (year == 1) {
			this.roi = 4;   	// our yield is 4% on a 1 year money market
		}
		else if (year == 3) {
			this.roi = 5;   	// our yield is 5% on a 3 year money market
		}
		else if (year == 5) {	
			this.roi = 6;	  	// our yield is 6% on a 5 year money market
		}
	}
	public int getYears() {
		return years;
	}
	public void setYears(int years) {
		this.years = years;
	}
	
	public void calcReturn(double amount, int term){

		  double yearlyInterestPaid ;
		  double totalAmount = amount;
		  double intRate = 0;
		  int year = term;
			if (year == 1) {
				intRate = .04;   	// our yield is 4% on a 1 year money market
			}
			else if (year == 3) {
				intRate = .05;   	// our yield is 5% on a 3 year money market
			}
			else if (year == 5) {	
				intRate = .06;	  	// our yield is 6% on a 5 year money market
			}

		  for (int i = 1; i <= year; i++ ){
		    yearlyInterestPaid = totalAmount * intRate;
		    totalAmount = totalAmount + yearlyInterestPaid;
		  }
		  System.out.println("Get Excited! At the end of " + year + " years you will have $" + totalAmount);
	}
	
	@Override
	public String toString() {
		return  "Account Number: " + String.valueOf(getAcctNumber()) +
				" Account Holder Name: " + getAcctHolderName() +
				" Account Balance: " + String.valueOf(getAcctBalance()) +
				" Minimum Balance: " + String.valueOf(getMinBalance()) +
				" Interest Rate: " + String.valueOf(getRoi()) +
				"% Term (Years):" + String.valueOf(getYears());
	}
	

}
