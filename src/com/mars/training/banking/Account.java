package com.mars.training.banking;
import java.util.Scanner;


public class Account {
	
	Scanner sc = new Scanner(System.in);

	private int acctNumber;
	private String acctHolderName;
	private double acctBalance;
	private float minBalance;
	
	public int getAcctNumber() {
		return acctNumber;
	}
	public void setAcctNumber(int acctNumber) {
		this.acctNumber = acctNumber;
	}
	public String getAcctHolderName() {
		return acctHolderName;
	}
	public void setAcctHolderName(String acctHolderName) {
		this.acctHolderName = acctHolderName;
	}
	public double getAcctBalance() {
		return acctBalance;
	}
	public void setAcctBalance(double acctBalance) {
		this.acctBalance = acctBalance;
	}
	public float getMinBalance() {
		return minBalance;
	}
	public void setMinBalance(float minBalance) {
		this.minBalance = minBalance;
	}
	
	public double deposit(double depositAmt) {
		System.out.println("Thank you for your deposit of " + depositAmt);
		return this.acctBalance = this.acctBalance + depositAmt;
	}
	
	public double withdrawal(double withdrawalAmt) {
		if (this.acctBalance - this.minBalance < withdrawalAmt) {
			System.out.println("Your minimum balance is: " + this.minBalance);
			System.out.println("You cannot withdraw more than: " + (this.acctBalance - this.minBalance));
			return this.acctBalance;
		}
		else {
			System.out.println("Thank you for your withdrawal of " + withdrawalAmt);
			return this.acctBalance = this.acctBalance - withdrawalAmt;
		}
	}
	
	public void checkBalance() {
		System.out.println("Your current balance is: " + this.acctBalance);
	}
	
	public void fakeLogin() {
		System.out.println("Please scan your ATM card to continue");
		
		// pretend to read card scan
		try {
	        Thread.sleep(1000);
	    } catch (InterruptedException e) {
	        Thread.currentThread().interrupt();  //set the flag back to true
	    } 
		System.out.println("Loading...");
		
		//pretend to look up account
		try {
	        Thread.sleep(2000);
	    } catch (InterruptedException e) {
	        Thread.currentThread().interrupt();  //set the flag back to true
	    } 
		
		// ask for pretend password
		System.out.println("Please enter your password for account 945284");
		String throwAwayString = sc.next();
		
		// pretend to read password, and compare to password on file
		System.out.println("Retrieving account information...");
		
		// pretend to retrieve cardholder information
		try {
	        Thread.sleep(4000);
	    } catch (InterruptedException e) {
	        Thread.currentThread().interrupt();  //set the flag back to true
	    } 
		
		// fill with fake info
		this.acctNumber = 945284;
		this.acctHolderName = "Laura Krumholz";
		this.acctBalance = 3000.00;
		
	}
}
