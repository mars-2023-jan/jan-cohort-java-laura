package com.mars.training;

public class MyClass {
	
	public static void main(String args[]) {
		System.out.println("Main Method");
	}
	
	// static block gets executed before the main method
	// initialize resources, connect to database, load file
	static {
		System.out.println("Static Block");
	}
	
	// with multiple static block they execute in order
	// but all before main
	static {
		System.out.println("Second Static Block");
	}
	
	// garbage collector frees up memory by deleting unused resources
	// but you can also close yourself
	// for example, sc.close() to close the scanner from yesterday
	
	// why is String capitalized like a class?
	// string is an array of characters, class in Java
	// string literal: if str and str2 both contain hello, they take up one space, literally pointing to same object in pool
	// string object: if contents are same, they still are different objects
	
	public void Strings() {
		String str = "hello"; // string literal
		String str2 ="hello";
		String strObj1 = new String("world"); // string object
		String strObj2 = new String("world");
		
		System.out.println(str == str2); // true
		System.out.println(strObj1 == strObj2); //false
		System.out.println (strObj1.equals(strObj2)); // true
		
		System.out.println(str.concat("world")); // hello world
	
		str.concat("world");
		System.out.println(str); // hello - immutable
		str = str.concat("world"); 
		System.out.println(str); // hello world - reassigned
		
		StringBuffer sb = new StringBuffer("abc");
		sb.append(" Technologies");
		System.out.println(sb); // abc Technologies - StringBuffer is mutable
		
		// before you write logic, look in the string method api to see if it's already done for you
		
	 }
	
	
}
	