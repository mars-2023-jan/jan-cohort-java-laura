package com.mars.training.abstraction;

public class Triangle extends Shape implements ShapeInterface {
// can do both - extend a class and implement an interface
// creates a kind of multiple inheritance not possible otherwise
	
	private double base;
	private double height;
	
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	
	//implement override so we don't have to make this whole class abstract
	@Override
	public double area() {
		
		return 0.5 * base * height;
	}
	
	
	
}
