package com.mars.training.abstraction;

import java.util.Scanner;

public class TestCarFactory {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the type of car you would like to build:");
				
		String carType = sc.next().toUpperCase();
		//converting our string to enum because method wants enum as parameter
		CarFactory.buildCar(CarType.valueOf(carType));
		
		Car c1 = new SmallCar();
		// Car c1 new Car() will not work because Car is an abstract
		// superclass reference can refer to subclass object
		
		System.out.println(c1.getDetails());
		
		// example of Static Polymorphism
		// always associated with Method Overloading
		System.out.println(c1.getDetails("New Car"));
		
		// example of Dynamic Polymorphism
		// always associated with Method Overriding
		// superclass reference must refer to subclass object
		// if you have the same method in super class and in subclass
		// the subclass will take preference (more specific)
		// see getDetails in Car and SmallCar
	}

}

//Factory Design Pattern: 
// TestCarFactory is like the client side, it is unaware of what happens on the server side
// CarFactory and the car classes are like the server side, they actually build the car and deliver it