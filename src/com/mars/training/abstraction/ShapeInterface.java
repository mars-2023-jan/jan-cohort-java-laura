package com.mars.training.abstraction;

public interface ShapeInterface {

	double area();
	
}


//interface is 100% abstract class
//can be used instead of making a class abstract
//to create, you choose interface instead of class when creating new
//allows us to simulate multiple inheritance (which is not possible in Java 
//extend a class and implement an interface at the same time
//ex: public class Triangle extends Shape implements ShapeInterface