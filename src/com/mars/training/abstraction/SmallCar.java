package com.mars.training.abstraction;

public class SmallCar extends Car{
	
	public SmallCar() {
		super(CarType.SMALL);
		construct();
	}

	@Override
	protected void construct() {
		System.out.println("Building small car...");
		
	}
	
	@Override
	public String getDetails() {
		return "Small Car";
	}
	//this is also present in Car but with different text
	
}

