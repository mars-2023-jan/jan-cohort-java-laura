//abstraction: hide the implementation and show only the user interface

package com.mars.training.abstraction;

public abstract class Shape {
// if even one method in a class is abstract, then the class should be abstract
// you can still have regular methods in an abstract class
// users will call method on shape and never see triangle

	public abstract double area();
	// abstract: define a method but do not give it any functionality
		
}
