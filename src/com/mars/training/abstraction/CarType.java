package com.mars.training.abstraction;

public enum CarType {
	// enum only contains constants
	
	SMALL, SEDAN, LUXURY

}
