package com.mars.training.abstraction;

public class LuxuryCar extends Car{
	
	public LuxuryCar() {
		super(CarType.LUXURY);
		construct();
	}

	@Override
	protected void construct() {
		System.out.println("Constructing luxury car...");
		System.out.println("Adding luxury car accessories!");
	}

}
