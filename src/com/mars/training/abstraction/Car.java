package com.mars.training.abstraction;

public abstract class Car {
	
	private CarType model;
	
	public CarType getModel() {
		return model;
	}

	public void setModel(CarType model) {
		this.model = model;
	}
	
	public Car() {
		
	}

	public Car(CarType model) {
		this.model = model;
	}
	
	// Constructor Overloading - two constructors with different parameters
	
	protected abstract void construct();
	// use protected when used by classes
	
	public String getDetails() {
		return "Car Details:";
	}
	
	public String getDetails(String name) {
		return name;
	}
	
	// you can have two methods with same name as long as parameter list is different
	// this is called Method Overloading

}
