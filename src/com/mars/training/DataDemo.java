package com.mars.training;

import java.util.Scanner;


public class DataDemo {

	public static void main(String[] args) {
	
		byte b = 25;
		int i = b;			//implicit typecast
		short s = (short)i; //explicit typecast
		float f = 30.6f;
		
//		int sum = add(23, 25);  will not work
		
//instead do
//		DataDemo demo1 = new DataDemo();
//		int sum = demo1.add(23, 25); // plus add method below
		
//another method for numbers 	
		Scanner sc = new Scanner(System.in);
//		randNum = (int)Math.floor(Math.random() * (maxNum - minNum + 1) + minNum);
//		int firstNumber = sc.nextInt();
//		System.out.println("Input second number: ");
//		int secondNumber = sc.nextInt();*/
//		
//and for strings
		System.out.println("Enter your name: ");
		String name = sc.nextLine();
		System.out.println("Your name is: " + name);
		
		
//		DataDemo demo1 = new DataDemo();
//		int sum = demo1.add(firstNumber, secondNumber);
//		System.out.println("Sum is: " + sum);
		
		

	}

	private int add(int a, int b) {
		return a + b;
	}
}
