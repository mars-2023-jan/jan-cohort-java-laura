package com.mars.training.Exception;

public class MyException extends Exception{

	public MyException( String msg) {
		super(msg);
	}

}
