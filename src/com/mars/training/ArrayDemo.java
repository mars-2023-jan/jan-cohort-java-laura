package com.mars.training;

import java.util.Scanner;


public class ArrayDemo {
	
	public static void main(String[] args) {
		int[] numbers = new int[5]; //reserves 20 bytes of contiguous memory 4x5, default value provided
		System.out.println(numbers[1]); // returns 0 - default value of integer
	
		int[] newNum = {10, 20, 30}; // creates array of 12 bytes with these values

		numbers[0] = 14;
		numbers[4] = 20;
		System.out.println(numbers[1]); // 0
		System.out.println(numbers[0]); // 14
		System.out.println(numbers[4]); // 20
		
		System.out.println(numbers[5]); // out of bound exception error
		
		//best way to populate or traverse array is for loop
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter five numbers for the array: ");
		for (int i = 0; i < numbers.length; i++) {
			// make sure scanner imported
			int num = sc.nextInt();
			numbers[i] = num;	
		}
		
		System.out.println("Five numbers entered are: ");
		for (int i = 0; i < numbers.length; i++) {
			System.out.println(numbers[i]);	
		}
		
		
	}
}
	
