package com.mars.training.collection;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;



public class CollectionDemo {

	public static void main(String[] args) {
		
//		Set<String> names = new HashSet<>();
		Set<Integer> names = new TreeSet<>(); // integer here is a wrapper class 
		
		
//		Set<Student> students = new HashSet<>();
//		Set<Student> students = new TreeSet<>(new IdComparator());  //error, which field to sort on? 
												  //need to add comparable interface
	
		List<Student> studList = new ArrayList<>(); //added to make it a list
		
		Set<Student> students = new TreeSet<>(new IdComparator());
		
//		students.add(new Student(105, "Sara", "22" ));
//		students.add(new Student(103, "Dave", "21" ));
//		students.add(new Student(101, "Mark", "23" ));
//		students.add(new Student(102, "Sara", "22" ));
//		
//		studList.add(new Student(105, "Sara", "22", "A" ));
//		studList.add(new Student(103, "Dave", "21", "C" ));
//		studList.add(new Student(101, "Mark", "23", "A" ));
//		studList.add(new Student(102, "Sara", "22", "B" ));
//		
//		System.out.println(studList.get(0));
//		studList.add(2, new Student(104, "Amy", "22", "F" ));
		
		List<Student> aList = new ArrayList<>(); //added to make it a list
		
//		Map<Integer, String> myMap = new HashMap<>();
//		myMap.put(101, "Vinny");
//		myMap.put(102, "Mark");
//		myMap.put(103, "Kate");
		
//		System.out.println(myMap.get(101));
//		
//		for(Map.Entry<Integer, String> m: myMap.entrySet()) {
//			System.out.println(m.getKey() + ":" + m.getValue());
//		}
		

		
		List<Student> stud = new ArrayList<>(); //added to make it a list
		
		stud.add(new Student(105, "Sara", "22", "A" ));
		stud.add(new Student(103, "Dave", "21", "C" ));
		stud.add(new Student(101, "Mark", "23", "A" ));
		stud.add(new Student(102, "Sara", "22", "B" ));
	
		Map<Integer, Student> studMap = new HashMap<>();
		
		for(Map.Entry<Integer, Student> m: studMap.entrySet()) {
			studMap.put(studMap.getKey());
		}
		
		
		for(Map.Entry<Integer, String> m: studMap.entrySet()) {
			System.out.println(studMap.entrySet() );
		}
		
		//primitives (byte, short, int, double) can't be in a collection
//		names.add(3); // each primitive has corresponding java class
//		names.add(5); // wrapper class
//		names.add(2); // primitive to wrapper class --> boxing
//		names.add(3); // wrapper to primitive --> unboxing
//		
//		for (Student stud : studList) {
//			var grade = stud.getGrade();
//			if (grade == "A") {
//				aList.add(stud);
//			}
//		}
//		
//		for (Student aStud : aList) {
//			System.out.println(aStud);
//		}
//		
//		studList.remove(1);
//		
//		List num = Arrays.asList(2, 5, 7, 9);
		
//		Collections.sort(studList, new NameComparator()); // added to sort list
		
//		for (Student stud : students) {
//			System.out.println(stud.getStudId() + ":" + stud.getStudName());
//		} 
		// gives duplicate Sara because it is a user created object 
		// need to modify student with @ overrides to compare id and values
		
//		names.add(new String("Tom"));
//		names.add(new String("Sara"));
//		names.add(new String("Lee"));
//		names.add(new String("Tom"));
		// knows not to duplicate Tom
		

//		can do this but should not do
//  	Set names = new HashSet();
//		names.add("Tom");
//		names.add(21);
//		names.add(new Car());
//		
//		Set<String> names = new HashSet<>();  //generics
//		
//		names.add("Tom");
//		names.add("Sara");
//		names.add("Rohan");
//		names.add("Sara");  // won't display because it is not unique
//		
//		for (String name : names) {
//			System.out.println(name);
//		}
//
//		Set<String> names2 = new TreeSet<>();  //ordered
//		
//		names.add("Tom");
//		names.add("Sara");
//		names.add("Rohan");
//		names.add("Sara");  // won't display because it is not unique
//		
//		for (String name : names2) {
//			System.out.println(name);
//		}
//
//		
//		Iterator<String> it = names.iterator();
//		
//		while(it.hasNext()) {
//			System.out.println(it.next());
//		}
//		
		

	}

}
