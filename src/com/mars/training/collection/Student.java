package com.mars.training.collection;

import java.util.Objects;

public class Student {
//public class Student implements Comparable<Student>{
// add comparable interface to add sort field - also add override
// the implements comparable would need to be changed if you want to change sort 
// better to externalize sort
	
	private int studId;
	private String studName;
	private String studAge;
	private String grade;
	
	public Student(int studId, String studName, String studAge, String grade) {
		super();
		this.studId = studId;
		this.studName = studName;
		this.studAge = studAge;
		this.grade = grade;
	}
	 

	public int getStudId() {
		return studId;
	}
	public void setStudId(int studId) {
		this.studId = studId;
	}
	public String getStudName() {
		return studName;
	}
	public void setStudName(String studName) {
		this.studName = studName;
	}
	public String getStudAge() {
		return studAge;
	}
	public void setStudAge(String studAge) {
		this.studAge = studAge;
	}

	// this checks for equal id
	@Override
	public int hashCode() {
		return studId;
	}

	// this checks to make sure the other fields are also equal
	// if so, it just returns the same hashcode as the first (not a new item)
	@Override
	public boolean equals(Object obj) {
		Student other = (Student) obj;
		return Objects.equals(studAge, other.studAge) && studId == other.studId
				&& Objects.equals(studName, other.studName);
	}


	@Override
	public String toString() {
		return "Student [studId=" + studId + ", studName=" + studName + ", studAge=" + studAge + ", studGrade =" + grade +  "]";
	}


	public String getGrade() {
		return grade;
	}


	public void setGrade(String grade) {
		this.grade = grade;
	}

	// this allows us to sort
//	@Override
//	public int compareTo(Student o) {
//		//return this.studName.compareTo(o.getStudName());
//		if(this.studId > o.getStudId()) 
//			return 1;
//		else if (this.studId == o.getStudId())
//			return 0;
//		else
//			return -1; 		}
		// compareTo is a built in string function that takes parameter 1, 0, -1
		// TreeSet, looks for Student object, needs comparable, needs compareto
		// gets called automatically every time we add to the object
	}
		 
	


