package com.mars.training.inheritance;

public class Car extends Vehicle {

	String brand;
	int capacity;
	
	public Car() {
		super(); // optional, Java will enforce
		System.out.println("Car Constructor Called");
	}
	
	public String getVehicleDetails() {
		 return super.getVehicleDetails() + " Brand: " + this.brand + " Capacity: " + this.capacity;
	}
}
