package com.mars.training.inheritance;

public class Vehicle {
	String vehicleType;
	String color;
	String make;
	boolean isRegistered;
	
	public Vehicle() {
		System.out.println("Vehicle Constructor Called");
	}
	
	public String getVehicleDetails() {
		return "Vehicle: "+ this.vehicleType+ " Color: " + this.color + 
				" Make: " + this.make + " Registered? " + this.isRegistered;
	}

}


//OOPs principles:
	// 1. encapsulation - wrapping up of data and methods using a class
	// 2. inheritance - acquiring or inheriting super class variables or methods
	//    multi-level inheritance
	//    single inheritance
	//	  multiple inheritance - not supported by java (extends and & b)
