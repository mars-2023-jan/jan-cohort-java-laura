package com.mars.training.inheritance;

public class InheritanceDemo {

	public static void main(String[] args) {
		
		SedanCar sedanCar1 = new SedanCar();
		
		sedanCar1.vehicleType = "Sedan Car";
		sedanCar1.make = "Ford";
		sedanCar1.color = "Red";
		sedanCar1.isRegistered = true;
		sedanCar1.capacity = 4;
		sedanCar1.brand = "Festiva";
		
		System.out.println(sedanCar1.getVehicleDetails());
	}
}

// Whenever a child class object will be created, make sure parent class
// object is present