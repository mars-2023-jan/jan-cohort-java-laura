package com.mars.training.inheritance;

public class SedanCar extends Car{

	public SedanCar() {
		super();  //optional, Java will enforce
		System.out.println("Sedan Car Constructor Called");
	}
}
