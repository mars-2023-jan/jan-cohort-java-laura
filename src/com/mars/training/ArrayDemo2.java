package com.mars.training;

import java.util.Scanner;

public class ArrayDemo2 {
	
	public static void main(String [] args) {
		
		int[][] y = new int [3][3]; // creates two dimension array
		y[0][0] = 12;
		
		int[][] x = new int [3][]; // don't have to specify columns
		y[0] = new int[2]; // makes the columns
		y[1] = new int[3]; // rows can have different numbers of columns
		y[2] = new int[2];
		
		for (int i = 0; i < y.length; i++) {
			for (int j = 0; j < y[i].length; j++) {
				y[i][j] = i*2;
				System.out.print(y[i][j]);
			}
			System.out.println();
		}
		
	}

}


// Learning Java is fun. Java has a lot of things.
// find repeating (Java)
// logic:
// save each word using "split" with space delimiter

Scanner sc - new Scanner(System.in);
String [] str = sc.nextLine().split(" ")
for (int i = 0, i < str.length, i++) {
	System,out.println(str[i])	
}

// that saves the string in individual worlds
// now go one by one and check for match
// if match, print out

// also, create a class name Shop 
// with attributes shopName - string, shopAddress - string, products - string array
// getter, setter, constructors
// parameterized constructor for Shop
// create a checkProductAvailability method which will
// take product name as parameter
// returns whether the product is available, and which shop
// create a Main method which will
// take inputs from the user with Scanner and search for the productname
