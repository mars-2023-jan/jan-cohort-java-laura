package com.mars.assignments.lowest;

public class Employee {

	private String empName;
	private String empAddress;
	public int empSalary;
	private int empId;
	private String empRole;
	static String companyName;
	
	
	public void setData(int empId, String empName, String empAddress, String empRole, int empSalary) {
		this.empId = empId;
		this.empName = empName;
		this.empAddress = empAddress;
		this.empRole = empRole;
		this.empSalary = empSalary;
	}
	

	public void getEmpDetails() {
		System.out.println( "Company Name: "+companyName+" Employee Name: "+empName+" Emp Id: "
				+empId+ " Address: "+empAddress+ " Employee Role: "+empRole+ " Salary: "+empSalary);
	}


}



