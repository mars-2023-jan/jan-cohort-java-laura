package com.mars.assignments.lowest;

public class LowestEmployee {
	public static void main(String args[]) {
		
		Employee.companyName = "My Company";
		
		Employee[] arr;

		arr = new Employee[5];
		
		arr[0] = new Employee();
		arr[1] = new Employee();
		arr[2] = new Employee();
		arr[3] = new Employee();
		arr[4] = new Employee();
		
		arr[0].setData(101, "John", "Chicago", "Intern", 10000);
		arr[1].setData(102, "Sara", "Miami", "Sales", 70000);
		arr[2].setData(103, "Pete", "New York", "Sales", 65000);
		arr[3].setData(104, "Andy", "Phoenix", "CEO", 100000);
		arr[4].setData(105, "Laura", "Nashville", "Marketing", 50000);
		
		System.out.println("The employees at "+Employee.companyName+" are:");
		
		int min = 0;
		int minEmpIndex = 0;
		
		 for (int i = 0; i < arr.length; i++) {  
			arr[i].getEmpDetails();
		
			if ((arr[i].empSalary < min) || min == 0) {
				minEmpIndex = i;
				min = arr[i].empSalary;
			}
			
		 }
		
	
		System.out.println("The lowest paid Employee is:");
		arr[minEmpIndex].getEmpDetails();
		
				
    }
}