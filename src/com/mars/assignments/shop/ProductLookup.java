package com.mars.assignments.shop;
import java.util.Scanner;

public class ProductLookup {
	
	public static void main(String args[]) {
		
		Shop currentShop = new Shop();
		
		// hardcoding values for now
		currentShop.setShopName("Laura's Grocery Store");
		currentShop.setShopAddr("123 Main St. Sheboygan, WI");
		
		// cannot figure out how to "set" the products array into the Shop object
	    String[] products = {"milk", "eggs", "apples", "bread", "broccoli"};
		
		Scanner sc = new Scanner(System.in);
		boolean found = false;
		System.out.println("Enter the name of the product you would like to search for:");
		String prodSearch = sc.nextLine();
		
		for (int i = 0; i < products.length; i++){

			if (products[i].equals(prodSearch)) {
				found = true;
				break;
			} 
		}
		
		if (found) {
			System.out.println("You're in luck! " + currentShop.getShopName() + " at " + currentShop.getShopAddr() + " does have " + prodSearch + " in stock.");
		} else {
			System.out.println("Sorry... " + currentShop.getShopName() + " at " + currentShop.getShopAddr() + " does not have " + prodSearch + " in stock.");
		}
		
	}

}

