package com.mars.assignments.shop;

public class Shop {

	private String shopName;
	private String shopAddr;
	private String[] products;
		
	
	public void SetData(String shopName, String shopAddr, String[] products) {
		this.shopName = shopName;
		this.shopAddr = shopAddr;
		this.products = products;
	}
	
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddr() {
		return shopAddr;
	}
	public void setShopAddr(String shopAddr) {
		this.shopAddr = shopAddr;
	}
	public String[] getProducts() {
		return products;
	}
	public void setProducts(String[] products) {
		this.products = products;
	}
	

}

