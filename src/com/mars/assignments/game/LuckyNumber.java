package com.mars.assignments.game;
import java.util.Scanner;

public class LuckyNumber {

	public static void main(String[] args) {
	
		int minNum;
		int maxNum;
		int randNum;
		int guess;
		int numTries;
		int playAgain;
		
		minNum = 1;
		maxNum = 10;
		randNum = 0;
		guess = 0;
		numTries = 0;
		playAgain = 0;
		
		Scanner objScan = new Scanner(System.in);
		
		// tell user how to play
		System.out.println("Use your number keypad to play this game.");
		System.out.println("The game will think of a lucky number.");
		System.out.println("You will have three tries to guess the lucky number correctly.");
		System.out.println();
		
		// check that user wants to play (again)
		while (playAgain == 0) {
			
			//set variables, generate random number
			guess = 0;
			numTries = 0;
			
			System.out.println("Generating a random number between "+ minNum + " and " + maxNum + "...");
			System.out.println();
			randNum = (int)Math.floor(Math.random() * (maxNum - minNum + 1) + minNum);
			// uncomment for testing
			// System.out.println(randNum);
			
			while (numTries <= 3) {
				
				 	
				if ((guess != randNum) && (numTries < 3)) {
					// first guess, second guess, third guess
					numTries = numTries + 1;
					System.out.println("Try to guess my lucky number between "+ minNum + " and " + maxNum + ":");
					guess = objScan.nextInt();
				} else 
				if (guess == randNum) {	
					// they guessed the right number
					System.out.println();
					System.out.println("Congratulations! You guessed my lucky number in " +numTries+ " tries.");
					break;
				} else 
				if (numTries == 3){		
					// they didn't guess within 3 tries
					System.out.println();
					System.out.println("Sorry! You didn't guess my lucky number in " +numTries+ " tries.");	
					break;
				}
			   // end of if statement
				

			} // end of while loop	- number of tries is now > 3
			
			System.out.println("My lucky number is " + randNum);
			System.out.println();
			System.out.println("Enter 0 to play again or any other number to quit.");
			playAgain = objScan.nextInt();
		
		} //end of while loop, user didn't press 0
		
		System.out.println("Goodbye!");
	}
}
