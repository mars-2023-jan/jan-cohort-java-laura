package com.mars.assignments.contacts;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class ContactsManager {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		Set<Contact> contactList = new TreeSet<>(new ContactComparator());
		
		// add some initial values to list
		contactList.add(new Contact("Laura", "Sheboygan, WI", "111-111-1111", "Laura".hashCode()));
		contactList.add(new Contact("Sujitha", "Green Bay, WI", "123-123-1234", "Sujitha".hashCode()));
		contactList.add(new Contact("Ancy", "Milwaukee, WI", "222-222-2222", "Ancy".hashCode()));
		contactList.add(new Contact("Richard", "Milwaukee, WI", "345-345-3456", "Richard".hashCode()));
		contactList.add(new Contact("Alka", "India", "444-555-6666", "Alka".hashCode()));
		
		// display all contacts for testing
		System.out.println("List of all current contacts:");
		for (Contact Contact : contactList) {
			System.out.println(Contact.getcName() + " : " + Contact.getcAddress() + " : " + Contact.getcPhone());
		}
		System.out.println();
		
		var choice = 0;
		
		do {
			System.out.println("Press 1 to Search | Press 2 to Add | Press 3 to Edit | Press 4 to Delete");
			choice = sc.nextInt();
			
			if (choice ==1 ) {
				//do search stuff
				System.out.println("Enter name to search for in Contacts:");
				var found = false;
				var name = sc.next();
				System.out.println("Searching for " + name + "...");
				for (Contact Contact : contactList) {
					if ((Contact.getcName().compareTo(name)) == 0){
						System.out.println("Found! " + Contact.getcName() + " : " + Contact.getcAddress() + " : " + Contact.getcPhone());
						found = true;
					}			
				}
				if (!(found)) {
					System.out.println(name + " is not found in your contacts");
				}
			} 
			
			else if (choice == 2) {
				// do add stuff
				var addName = "";
				var addPhone = "";
				var addAddress = "";
				
				System.out.println("Enter name of contact to add:");
				addName = sc.next() + sc.nextLine();
				System.out.println("Enter address of contact to add:");
				addAddress = sc.next() + sc.nextLine();
				System.out.println("Enter phone of contact to add:");
				addPhone = sc.next() + sc.nextLine();
				
				contactList.add(new Contact(addName, addAddress, addPhone, addName.hashCode()));
				
				// display all contacts for testing
				System.out.println("List of all current contacts:");
				for (Contact Contact : contactList) {
					System.out.println(Contact.getcName() + " : " + Contact.getcAddress() + " : " + Contact.getcPhone());
				}
			} 
			
			else if (choice == 3) {
				// do edit stuff
				
				System.out.println("Enter name to search for in Contacts:");
				var found = false;
				var name = sc.next();
				System.out.println("Searching for " + name + "...");
				for (Contact Contact : contactList) {
					if ((Contact.getcName().compareTo(name)) == 0){
						System.out.println("Found! " + Contact.getcName() + " : " + Contact.getcAddress() + " : " + Contact.getcPhone());
						found = true;
						
						var addName = "";
						var addPhone = "";
						var addAddress = "";
						
						System.out.println("Enter new name:");
						addName = sc.next() + sc.nextLine();
						System.out.println("Enter new address:");
						addAddress = sc.next() + sc.nextLine();
						System.out.println("Enter new phone:");
						addPhone = sc.next() + sc.nextLine();
						
						Contact.setcName(addName);
						Contact.setcAddress(addAddress);
						Contact.setcPhone(addPhone);			
					}			
				}
				if (!(found)) {
					System.out.println(name + " is not found in your contacts");
				}	
				// display all contacts for testing
				System.out.println();
				System.out.println("Updated list of contacts:");
				for (Contact Contact : contactList) {
					System.out.println(Contact.getcName() + " : " + Contact.getcAddress() + " : " + Contact.getcPhone());
				}
			} 
			
			else if (choice == 4) {
				// do delete stuff
				System.out.println("Enter name to search for in Contacts:");
				var found = false;
				var name = sc.next();
				System.out.println("Searching for " + name + "...");
				for (Contact Contact : contactList) {
					if ((Contact.getcName().compareTo(name)) == 0){
						System.out.println("Found! " + Contact.getcName() + " : " + Contact.getcAddress() + " : " + Contact.getcPhone());
						System.out.println("Removing...");
						found = true;
						contactList.remove(Contact);
						break;
					}
				}
				if (!(found)) {
					System.out.println(name + " is not found in your contacts");
				}
				// display all contacts for testing
				System.out.println();
				System.out.println("Updated list of contacts:");
				for (Contact Contact : contactList) {
					System.out.println(Contact.getcName() + " : " + Contact.getcAddress() + " : " + Contact.getcPhone());
				}
			}	
			
		} while ((choice != 1) && (choice != 2) && (choice !=3) && (choice !=4));	
	}
}
