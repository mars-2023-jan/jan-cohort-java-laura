package com.mars.assignments.contacts;
import java.util.Comparator;


public class ContactComparator implements Comparator<Contact> {
	@Override
	public int compare(Contact o1, Contact o2) {
		int result = o1.getcName().compareTo(o2.getcName());
		if (result > 0)
			return 1;
		else if (result == 0)
			return 0;
		else
			return -1; 	
	}
}
