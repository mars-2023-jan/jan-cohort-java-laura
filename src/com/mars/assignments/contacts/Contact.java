package com.mars.assignments.contacts;

public class Contact {
	
	private String cName;
	private String cAddress;
	private String cPhone;
	private int cId;
	
	public Contact(String cName, String cAddress, String cPhone, int cId) {
		super();
		this.cName = cName;
		this.cAddress = cAddress;
		this.cPhone = cPhone;
		this.cId = cId;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getcAddress() {
		return cAddress;
	}

	public void setcAddress(String cAddress) {
		this.cAddress = cAddress;
	}

	public String getcPhone() {
		return cPhone;
	}

	public void setcPhone(String cPhone) {
		this.cPhone = cPhone;
	}

	public int getcId() {
		return cId;
	}

	public void setcId(int cId) {
		this.cId = cId;
	}
}
