package com.mars.assignments.student;
import java.util.Random;
import java.util.Scanner;

public class ResidentBuilder {
	
	public static void main (String[] args) {
		
	Scanner sc = new Scanner(System.in);
	
	Random rnd = new Random();
	int id = 100000 + rnd.nextInt(900000);
	String name = "";
	String phone = "";
	String major = "";
	String dorm = "";
	int room = 0;
	int saveOrEdit = 0;
	int whichUpdate = 0;

	
	System.out.println("Welcome to our dormitory assignment system!");
	System.out.println();
	
	//collect data
	System.out.println("Enter Student First & Last Name");	
	name = sc.next() + sc.nextLine();
	System.out.println("Enter Student Phone Number");	
	phone = sc.next() + sc.nextLine();
	System.out.println("Enter Student Major");	
	major = sc.next() + sc.nextLine();
	System.out.println("Enter Dorm Name");	
	dorm = sc.next() + sc.nextLine();
	System.out.println("Enter Room Number");	
	room = sc.nextInt();
	
	// assign to variables in object
    DormitoryAssign d1 = new DormitoryAssign();
	
	d1.setId(id);
	d1.setName(name);
	d1.setPhone(phone);
	d1.setMajor(major);
	d1.setDorm(dorm);
	d1.setRoom(room);
	
	DisplayData(d1.id, d1.name, d1.phone, d1.major, d1.dormitory, d1.roomNum);
	
	System.out.println("You may save this info OR you may edit any field");
	
	do {
		System.out.println("Press 1 to Save / Press 2 to Edit");
		saveOrEdit = sc.nextInt();
	}
	while ((saveOrEdit != 1) && (saveOrEdit != 2));
	
	if (saveOrEdit == 1) {
		//pretend to save
		System.out.println("Record Saved");
	}
	else if (saveOrEdit == 2) {
		//ask which they want to update
		do {
			System.out.println("Press 1 to Edit Name");
			System.out.println("Press 2 to Edit Phone Number");
			System.out.println("Press 3 to Edit Major");
			System.out.println("Press 4 to Edit Dorm");
			System.out.println("Press 5 to Edit Room Number");
			System.out.println("Press any other number to Quit and Save");
			whichUpdate = sc.nextInt();
		}
		while ((whichUpdate != 1) && (whichUpdate != 2) && (whichUpdate != 3) && (whichUpdate != 4) && (whichUpdate != 5));
		
			if (whichUpdate == 1) {
		    	//code name
				System.out.println();
				System.out.println("Enter New Name");
				name = sc.next() + sc.nextLine();
				d1.setName(name);
				DisplayData(d1.id, d1.name, d1.phone, d1.major, d1.dormitory, d1.roomNum);
			}
			else if (whichUpdate == 2) {
		    	//code phone
				System.out.println();
				System.out.println("Enter New Phone Number");
				phone = sc.next() + sc.nextLine();
				d1.setPhone(phone);
				DisplayData(d1.id, d1.name, d1.phone, d1.major, d1.dormitory, d1.roomNum);
			}
			else if (whichUpdate == 3) {
		    	//code major
				System.out.println();
				System.out.println("Enter New Major");
				major = sc.next() + sc.nextLine();
				d1.setMajor(major);
				DisplayData(d1.id, d1.name, d1.phone, d1.major, d1.dormitory, d1.roomNum);
			}
			else if (whichUpdate == 4) {
		    	//code dorm
				System.out.println();
				System.out.println("Enter New Dorm");
				dorm = sc.next() + sc.nextLine();
				d1.setDorm(dorm);
				DisplayData(d1.id, d1.name, d1.phone, d1.major, d1.dormitory, d1.roomNum);
			}
			else if (whichUpdate == 5) {
		    	//code room
				System.out.println();
				System.out.println("Enter New Room Number");
				room = sc.nextInt();
				d1.setRoom(room);
				DisplayData(d1.id, d1.name, d1.phone, d1.major, d1.dormitory, d1.roomNum);
			}
			else {
				//pretend to save
				System.out.println("Finished Editing");
			}
		
		System.out.println("Record Saved");
	}
	
		
	}

	private static void DisplayData(int id, String name, String phone, String major, String dormitory, int roomNum) {
		System.out.println();
		System.out.println("You entered the following student data:");
		System.out.println("Student ID (Generated) " + id);
		System.out.println("Student Name " + name);
		System.out.println("Student Phone Number " + phone);
		System.out.println("Student Major " + major);
		System.out.println("Student Dorm Assignment " + dormitory);
		System.out.println("Student Room Number " + roomNum);
		System.out.println();
	
	}
	
	
	
}
