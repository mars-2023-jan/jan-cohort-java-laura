package com.mars.assignments.student;


public class DormitoryAssign extends Student {
	
	String dormitory = "";
	int roomNum = 0;
	
	public String getDorm() {
		return dormitory;
	}
	public void setDorm(String dorm) {
		this.dormitory = dorm;
	}
	public int getRoom() {
		return roomNum;
	}
	public void setRoom(int room) {
		this.roomNum = room;
	}
	
}
