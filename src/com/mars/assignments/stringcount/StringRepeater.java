package com.mars.assignments.stringcount;
import java.util.Scanner;
import java.util.Arrays;


public class StringRepeater {

	public static void main(String [] args) {
		Scanner sc = new Scanner(System.in);
		String compareStr;
		int wordCount = 0;

		
		System.out.println("Enter your string now: ");
		String[] str = sc.nextLine().split(" ");

		
		System.out.println();
		System.out.println("The exact case sensitive, full word matches are: ");
		
	
		for (int i = 0; i < str.length; i++) {
			compareStr = str[i];
			for (int j = 0; j < str.length; j++){
				if (str[j].equals(compareStr)) {
					wordCount = wordCount+ 1;
				} 
			}
			System.out.println(compareStr +" has " + wordCount + " match(es)");
			wordCount = 0;
				
		}
	}
}
